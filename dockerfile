FROM mediawiki:1.35.0

# install missung stuff and php extensions
RUN apt-get update && apt-get install -y \
      vim \
      unzip \
      libzip-dev \
    && docker-php-ext-install zip

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php --version=1.10.16 --install-dir=/bin --filename=composer && \
	php -r "unlink('composer-setup.php');"

# update mediawiki extensions via composer
COPY composer.local.json .

RUN composer update --no-dev